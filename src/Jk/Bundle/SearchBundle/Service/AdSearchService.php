<?php

namespace Jk\Bundle\SearchBundle\Service;

use Doctrine\ORM\EntityManager;
use Jk\Bundle\SearchBundle\Entity\AdRepository;
use Symfony\Component\Form\Form;

class AdSearchService
{

    /**
     * @var AdRepository
     */
    private $adRepository;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->adRepository = $entityManager->getRepository('JkSearchBundle:Ad');
    }

    /**
     * @param Form $form
     * @return array
     */
    public function search(Form $form)
    {
        return $this->adRepository->findByAdSearchFormParameters($form->getData());
    }
} 