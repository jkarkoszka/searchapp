<?php

namespace Jk\Bundle\SearchBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Jk\Bundle\SearchBundle\Entity\AdRepository;
use Jk\Bundle\SearchBundle\Entity\AdType;
use Jk\Bundle\SearchBundle\Entity\MarketType;
use Jk\Bundle\SearchBundle\Entity\PropertyType;
use Jk\Bundle\SearchBundle\Entity\Region;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class AdRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var AdRepository
     */
    private $adRepository;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        self::bootKernel();
        $this->entityManager = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->adRepository = $this->entityManager->getRepository('JkSearchBundle:Ad');
    }

    /**
     * @dataProvider searchFormParametersProvider
     */
    public function testGetQueryBuilderSearchFormParameters($parameters, $expectedDQL)
    {
        $queryBuilder = $this->adRepository->getQueryBuilderSearchFormParameters($parameters);
        $this->assertEquals($expectedDQL, $queryBuilder->getDQL());
    }

    public function searchFormParametersProvider()
    {
        return array(
            array(array(), 'SELECT a FROM Jk\Bundle\SearchBundle\Entity\Ad a'),
            array(
                array('priceFrom' => 1, "priceTo" => 2),
                'SELECT a FROM Jk\Bundle\SearchBundle\Entity\Ad a WHERE a.price >= :priceFrom AND a.price <= :priceTo'
            ),
            array(
                array('areaFrom' => 1, "areaTo" => 2),
                'SELECT a FROM Jk\Bundle\SearchBundle\Entity\Ad a WHERE a.area >= :areaFrom AND a.area <= :areaTo'
            ),
            array(
                array('region' => $this->generateRegion("pomorskie")),
                'SELECT a FROM Jk\Bundle\SearchBundle\Entity\Ad a WHERE a.region = :region'
            ),
            array(
                array('propertyType' => $this->generatePropertyTypes(array("domy"))),
                'SELECT a FROM Jk\Bundle\SearchBundle\Entity\Ad a WHERE a.propertyType IN (:propertyType)'
            ),
            array(
                array('adType' => $this->generateAdType("sprzedam")),
                'SELECT a FROM Jk\Bundle\SearchBundle\Entity\Ad a WHERE a.adType = :adType'
            ),
            array(
                array('marketType' => $this->generateMarketType("pierwotny")),
                'SELECT a FROM Jk\Bundle\SearchBundle\Entity\Ad a WHERE a.marketType = :marketType'
            ),

        );
    }

    /**
     * @param $name
     * @return Region
     */
    private function generateRegion($name)
    {
        $region = new Region();
        $region->setValue($name);

        return $region;
    }

    /**
     * @param array $names
     * @return ArrayCollection
     */
    private function generatePropertyTypes(array $names)
    {
        $propertyTypes = new ArrayCollection();
        foreach ($names as $name) {
            $propertyType = new PropertyType();
            $propertyType->setValue($name);
            $propertyTypes->add($propertyType);
        }

        return $propertyTypes;
    }

    /**
     * @param $name
     * @return AdType
     */
    private function generateAdType($name)
    {
        $adType = new AdType();
        $adType->setValue($name);

        return $adType;
    }

    /**
     * @param $name
     * @return MarketType
     */
    private function generateMarketType($name)
    {
        $marketType = new MarketType();
        $marketType->setValue($name);

        return $marketType;
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->entityManager->close();
    }
}