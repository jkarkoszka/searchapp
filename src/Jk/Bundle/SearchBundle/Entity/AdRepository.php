<?php

namespace Jk\Bundle\SearchBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class AdRepository extends EntityRepository
{

    /**
     * @param array $parameters
     * @return array
     */
    public function findByAdSearchFormParameters(array $parameters)
    {
        $queryBuilder = $this->getQueryBuilderSearchFormParameters($parameters);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param array $parameters
     * @return QueryBuilder
     */
    public function getQueryBuilderSearchFormParameters(array $parameters)
    {
        $queryBuilder = $this->createQueryBuilder('a');
        $queryBuilder = $this->addSimpleEqualsConditions($queryBuilder, $parameters);
        $queryBuilder = $this->addRangeConditions($queryBuilder, $parameters);
        $queryBuilder = $this->addListConditions($queryBuilder, $parameters);

        return $queryBuilder;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $parameters
     * @return QueryBuilder
     */
    private function addSimpleEqualsConditions(QueryBuilder $queryBuilder, array $parameters)
    {
        if (!empty($parameters['city'])) {
            $queryBuilder->andWhere('a.city = :city')->setParameter("city", $parameters['city']);
        }
        if (!empty($parameters['region']) && $parameters['region'] instanceof Region) {
            $queryBuilder->andWhere('a.region = :region')->setParameter("region", $parameters['region']);
        }
        if (!empty($parameters['marketType']) && $parameters['marketType'] instanceof MarketType) {
            $queryBuilder->andWhere('a.marketType = :marketType')->setParameter(
                "marketType",
                $parameters['marketType']
            );
        }
        if (!empty($parameters['adType']) && $parameters['adType'] instanceof AdType) {
            $queryBuilder->andWhere('a.adType = :adType')->setParameter("adType", $parameters['adType']);
        }

        return $queryBuilder;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $parameters
     * @return QueryBuilder
     */
    private function addRangeConditions(QueryBuilder $queryBuilder, array $parameters)
    {
        if (!empty($parameters['priceFrom'])) {
            $queryBuilder->andWhere('a.price >= :priceFrom')->setParameter("priceFrom", $parameters['priceFrom']);
        }
        if (!empty($parameters['priceTo'])) {
            $queryBuilder->andWhere('a.price <= :priceTo')->setParameter("priceTo", $parameters['priceTo']);
        }
        if (!empty($parameters['areaFrom'])) {
            $queryBuilder->andWhere('a.area >= :areaFrom')->setParameter("areaFrom", $parameters['areaFrom']);
        }
        if (!empty($parameters['areaTo'])) {
            $queryBuilder->andWhere('a.area <= :areaTo')->setParameter("areaTo", $parameters['areaTo']);
        }

        return $queryBuilder;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $parameters
     * @return QueryBuilder
     */
    private function addListConditions(QueryBuilder $queryBuilder, array $parameters)
    {
        if (!empty($parameters['propertyType']) && $parameters['propertyType'] instanceof ArrayCollection) {
            /**
             * @var $propertyTypes ArrayCollection
             */
            $propertyTypes = $parameters['propertyType'];
            if ($propertyTypes->count() > 0) {
                $propertyTypesIds = array();
                foreach ($propertyTypes as $propertyType) {
                    $propertyTypesIds[] = $propertyType->getId();
                }
                $queryBuilder->andWhere('a.propertyType IN (:propertyType)')->setParameter(
                    "propertyType",
                    $propertyTypesIds
                );
            }
        }

        return $queryBuilder;
    }
}
