<?php

namespace Jk\Bundle\SearchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ad
 *
 * @ORM\Table(name="ads")
 * @ORM\Entity(repositoryClass="Jk\Bundle\SearchBundle\Entity\AdRepository")
 */
class Ad
{
    /**
     * @ORM\ManyToOne(targetEntity="AdType", inversedBy="ads")
     * @ORM\JoinColumn(name="ad_type_id", referencedColumnName="id")
     */
    protected $adType;
    /**
     * @ORM\ManyToOne(targetEntity="MarketType", inversedBy="ads")
     * @ORM\JoinColumn(name="market_type_id", referencedColumnName="id")
     */
    protected $marketType;
    /**
     * @ORM\ManyToOne(targetEntity="PropertyType", inversedBy="ads")
     * @ORM\JoinColumn(name="property_type_id", referencedColumnName="id")
     */
    protected $propertyType;
    /**
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="ads")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    protected $region;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;
    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;
    /**
     * @var integer
     *
     * @ORM\Column(name="area", type="integer")
     */
    private $area;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Ad
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Ad
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get area
     *
     * @return integer
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set area
     *
     * @param integer $area
     * @return Ad
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdType()
    {
        return $this->adType;
    }

    /**
     * @param mixed $adType
     */
    public function setAdType($adType)
    {
        $this->adType = $adType;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    /**
     * @param mixed $propertyType
     */
    public function setPropertyType($propertyType)
    {
        $this->propertyType = $propertyType;
    }

    /**
     * @return mixed
     */
    public function getMarketType()
    {
        return $this->marketType;
    }

    /**
     * @param mixed $marketType
     */
    public function setMarketType($marketType)
    {
        $this->marketType = $marketType;
    }
}
