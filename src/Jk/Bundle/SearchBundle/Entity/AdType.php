<?php

namespace Jk\Bundle\SearchBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdType
 *
 * @ORM\Table(name="ad_types")
 * @ORM\Entity(repositoryClass="Jk\Bundle\SearchBundle\Entity\AdTypeRepository")
 */
class AdType
{
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Ad", mappedBy="adType")
     */
    protected $ads;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    function __construct()
    {
        $this->ads = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getAds()
    {
        return $this->ads;
    }

    /**
     * @param ArrayCollection $ads
     */
    public function setAds($ads)
    {
        $this->ads = $ads;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return AdType
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Add ads
     *
     * @param \Jk\Bundle\SearchBundle\Entity\Ad $ads
     * @return AdType
     */
    public function addAd(\Jk\Bundle\SearchBundle\Entity\Ad $ads)
    {
        $this->ads[] = $ads;

        return $this;
    }

    /**
     * Remove ads
     *
     * @param \Jk\Bundle\SearchBundle\Entity\Ad $ads
     */
    public function removeAd(\Jk\Bundle\SearchBundle\Entity\Ad $ads)
    {
        $this->ads->removeElement($ads);
    }

    function __toString()
    {
        return $this->value;
    }
}
