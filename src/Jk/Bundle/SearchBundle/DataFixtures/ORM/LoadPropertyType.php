<?php

namespace Jk\Bundle\SearchBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Jk\Bundle\SearchBundle\Entity\PropertyType;

class LoadPropertyType extends AbstractFixture implements OrderedFixtureInterface
{

    private $propertyTypeValues = array(
        "mieszkania",
        "domy",
        "działki",
        "lokale użytkowe",
        "pokoje"
    );

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->propertyTypeValues as $propertyTypeValue) {
            $propertyType = new PropertyType();
            $propertyType->setValue($propertyTypeValue);
            $manager->persist($propertyType);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
} 