<?php

namespace Jk\Bundle\SearchBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Jk\Bundle\SearchBundle\Entity\AdType;

class LoadAdType extends AbstractFixture implements OrderedFixtureInterface
{

    private $adTypeValues = array(
        "sprzedaż",
        "kupno",
        "wynajem",
        "szukam do wynajęcia",
    );

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->adTypeValues as $adTypeValue) {
            $adType = new AdType();
            $adType->setValue($adTypeValue);
            $manager->persist($adType);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
} 