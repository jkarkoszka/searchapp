<?php

namespace Jk\Bundle\SearchBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Jk\Bundle\SearchBundle\Entity\Ad;
use Jk\Bundle\SearchBundle\Entity\AdType;
use Jk\Bundle\SearchBundle\Entity\MarketType;
use Jk\Bundle\SearchBundle\Entity\PropertyType;

class LoadAd extends AbstractFixture implements OrderedFixtureInterface
{

    const NUMBER_OF_ADS_TO_GENERATE = 100;
    /**
     * @var AdType[]
     */
    private $adTypes;
    /**
     * @var MarketType[]
     */
    private $marketTypes;
    /**
     * @var Region[]
     */
    private $regions;
    /**
     * @var PropertyType[]
     */
    private $propertyTypes;
    private $cities = array(
        "Gdańsk",
        "Sopot",
        "Gdynia",
        "Warszawa",
    );

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->loadRelatedEntities($manager);
        $this->generateAds($manager);
    }

    /**
     * @param ObjectManager $manager
     */
    private function loadRelatedEntities(ObjectManager $manager)
    {
        $this->adTypes = $manager->getRepository("JkSearchBundle:AdType")->findAll();
        $this->marketTypes = $manager->getRepository("JkSearchBundle:MarketType")->findAll();
        $this->regions = $manager->getRepository("JkSearchBundle:Region")->findAll();
        $this->propertyTypes = $manager->getRepository("JkSearchBundle:PropertyType")->findAll();
    }

    /**
     * @param ObjectManager $manager
     */
    private function generateAds(ObjectManager $manager)
    {
        for ($i = 0; $i < self::NUMBER_OF_ADS_TO_GENERATE; $i++) {
            $ad = new Ad();
            $ad->setAdType($this->getRandomElementOfArray($this->adTypes));
            $ad->setMarketType($this->getRandomElementOfArray($this->marketTypes));
            $ad->setPropertyType($this->getRandomElementOfArray($this->propertyTypes));
            $ad->setRegion($this->getRandomElementOfArray($this->regions));
            $ad->setCity($this->getRandomElementOfArray($this->cities));
            $ad->setArea(mt_rand(10, 50));
            $ad->setPrice(mt_rand(100000, 500000));
            $manager->persist($ad);
        }
        $manager->flush();
    }

    private function getRandomElementOfArray($array)
    {
        shuffle($array);

        return reset($array);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }
} 