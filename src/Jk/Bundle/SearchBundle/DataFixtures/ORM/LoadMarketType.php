<?php

namespace Jk\Bundle\SearchBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Jk\Bundle\SearchBundle\Entity\MarketType;

class LoadMarketType extends AbstractFixture implements OrderedFixtureInterface
{

    private $marketTypeValues = array(
        "pierwotny",
        "wtórny",
    );

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->marketTypeValues as $marketTypeValue) {
            $marketType = new MarketType();
            $marketType->setValue($marketTypeValue);
            $manager->persist($marketType);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
} 