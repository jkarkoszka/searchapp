<?php

namespace Jk\Bundle\SearchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchAdType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', 'text', array('required' => false))
            ->add('priceFrom', 'text', array('required' => false))
            ->add('priceTo', 'text', array('required' => false))
            ->add('areaFrom', 'text', array('required' => false))
            ->add('areaTo', 'text', array('required' => false))
            ->add('adType', 'entity', array('class' => 'Jk\Bundle\SearchBundle\Entity\AdType', 'required' => true))
            ->add(
                'marketType',
                'entity',
                array(
                    'class' => 'Jk\Bundle\SearchBundle\Entity\MarketType',
                    'empty_value' => 'dowolny',
                    'empty_data' => null,
                    'required' => false
                )
            )
            ->add(
                'propertyType',
                'entity',
                array(
                    'class' => 'Jk\Bundle\SearchBundle\Entity\PropertyType',
                    "expanded" => true,
                    "multiple" => true,
                    'required' => false
                )
            )
            ->add(
                'region',
                'entity',
                array(
                    'class' => 'Jk\Bundle\SearchBundle\Entity\Region',
                    'empty_value' => 'cała Polska',
                    'empty_data' => null,
                    'required' => false
                )
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'jk_bundle_searchbundle_ad';
    }
}
