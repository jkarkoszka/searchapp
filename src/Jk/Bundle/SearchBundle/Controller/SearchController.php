<?php

namespace Jk\Bundle\SearchBundle\Controller;

use Jk\Bundle\SearchBundle\Form\SearchAdType;
use Jk\Bundle\SearchBundle\Service\AdSearchService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SearchController extends Controller
{

    /**
     * @var AdSearchService
     */
    private $adSearchService;

    /**
     * @Route("/", name="form")
     * @Method("GET")
     * @Template()
     */
    public function formAction()
    {
        $form = $this->getSearchAdForm();

        return $this->render(
            'JkSearchBundle:Search:form.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function getSearchAdForm()
    {
        return $this->createForm(new SearchAdType());
    }

    /**
     * @Route("/", name="result")
     * @Method("POST")
     * @Template()
     */
    public function resultAction(Request $request)
    {
        $this->adSearchService = $this->get('jk_search.ad_search_service');
        $form = $this->getSearchAdForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $ads = $this->adSearchService->search($form);

            return $this->renderResults($ads);
        }

        return $this->redirect($this->generateUrl('form'));
    }

    private function renderResults(array $ads)
    {
        if (empty($ads)) {
            return $this->render('JkSearchBundle:Search:no-result.html.twig');
        }

        return $this->render(
            'JkSearchBundle:Search:result.html.twig',
            array(
                'ads' => $ads,
            )
        );
    }
}
